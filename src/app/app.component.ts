import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ng6-proj1';
  estIdentif = false;
  constructor() { setTimeout( () => { this. estIdentif = true; }, 4000 ); } 
  descDiplome1 = "Baccalauréat"; descDiplome2 = "Licence"; 
  onObtenir() { this.diplomeEtat='obtenu';
  
} 
diplomeEtat="non obtenu";
langues=[
  {nom: 'Arabe',niveau : 'Très bien'},
  {nom: 'Français',niveau : 'Très bien'},
  {nom: 'Anglais',niveau : 'Bien'}];
  
}
