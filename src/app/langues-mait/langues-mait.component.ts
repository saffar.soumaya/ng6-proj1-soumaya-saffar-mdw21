import { Component, Input ,OnInit } from '@angular/core';

@Component({
  selector: 'app-langues-mait',
  templateUrl: './langues-mait.component.html',
  styleUrls: ['./langues-mait.component.css']
})
export class LanguesMaitComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  @Input() "languesma" : string;
  @Input() "languesniv" : string;
}
