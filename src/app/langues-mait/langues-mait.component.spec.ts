import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LanguesMaitComponent } from './langues-mait.component';

describe('LanguesMaitComponent', () => {
  let component: LanguesMaitComponent;
  let fixture: ComponentFixture<LanguesMaitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LanguesMaitComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LanguesMaitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
