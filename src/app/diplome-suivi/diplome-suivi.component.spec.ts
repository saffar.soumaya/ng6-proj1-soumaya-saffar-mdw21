import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DiplomeSuiviComponent } from './diplome-suivi.component';

describe('DiplomeSuiviComponent', () => {
  let component: DiplomeSuiviComponent;
  let fixture: ComponentFixture<DiplomeSuiviComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DiplomeSuiviComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DiplomeSuiviComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
