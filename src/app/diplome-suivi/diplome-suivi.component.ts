
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-diplome-suivi',
  templateUrl: './diplome-suivi.component.html',
  styleUrls: ['./diplome-suivi.component.css']
})
export class DiplomeSuiviComponent implements OnInit {

  constructor() {}

  ngOnInit(): void {
  }
  @Input()descDiplome1 : string = 'Baccalauréat';
   descDiplome2 : string = 'Licence';

  getEtat() { return this.diplomeEtat; }
  @Input() diplomeEtat : string = 'non obtenu';

  getColor(){
    if(this.diplomeEtat == 'obtenu')
    { return 'green'; }
    else if(this.diplomeEtat == 'non obtenu')
    { return 'red'; }


  }
}
